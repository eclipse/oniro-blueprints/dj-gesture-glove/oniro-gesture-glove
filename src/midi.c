/**@file
 * @brief HTS Service sample
 */

/*
 *Copyright (c) 2020 SixOctets Systems
 *Copyright (c) 2019 Aaron Tsui<aaron.tsui@outlook.com>
 *
 *SPDX-License-Identifier: Apache-2.0
 */
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <drivers/sensor.h>
#include <logging/log.h>
#include <sys/printk.h>
#include <sys/byteorder.h>
#include <zephyr.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include "midi.h"
#define LOG_MODULE_NAME midi
LOG_MODULE_REGISTER(LOG_MODULE_NAME);


uint8_t msg[5];

static void midi_ccc_cfg_changed(const struct bt_gatt_attr *attr, uint16_t value) {}

static ssize_t on_receive(struct bt_conn *conn, 
			const struct bt_gatt_attr *attr,
			const void *buf, 
			uint16_t len, 
			uint16_t offset, 
			uint8_t flags)
{
return 0;
}

static ssize_t on_read(struct bt_conn *conn, 
			const struct bt_gatt_attr *attr,
			void *buf,
			uint16_t len,
			uint16_t offset)
{
return 0;
}

/*BLE MIDI Service Declaration */
BT_GATT_SERVICE_DEFINE(midi_svc,                                               
BT_GATT_PRIMARY_SERVICE(BT_UUID_MIDI_SERVICE),                          
	BT_GATT_CHARACTERISTIC(BT_UUID_MIDI_IO,                        
							BT_GATT_CHRC_NOTIFY | BT_GATT_CHRC_READ | 
							BT_GATT_CHRC_WRITE_WITHOUT_RESP,            
							BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,     
		on_read, on_receive, &msg),
	BT_GATT_CCC(midi_ccc_cfg_changed, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE)
);

int midi_note(struct bt_conn *conn, uint8_t chan, uint8_t note, uint8_t state)
{

	uint32_t timestamp = k_cycle_get_32() & 0x00001fff;
	msg[0]    = 0x80 | (timestamp >> 7);       // Header byte = 0b10xxxxxx where xxxxxxx is top 6 bits of timestamp
    msg[1] = 0x80 | (timestamp & 0x003f);
	msg[2] = chan;	// Status byte = 0b1sssnnnn where sss is message type and nnnn is channel
	msg[3] = note;	// Setting the note parameter
	msg[4] = state;	// Setting the velocity parameter

	// Sending the assembled midi message
	int rc;
	rc = bt_gatt_notify(conn, &midi_svc.attrs[1], &msg, 5);

	return rc == -ENOTCONN ? 0 : rc;

}
